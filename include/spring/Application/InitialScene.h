#pragma once

#include "spring\Framework\IScene.h"
#include "qobject.h"

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:
		InitialScene(const std::string& ac_szSceneName);

		virtual void createScene();

		virtual void release();

		~InitialScene();

	public slots:

		void mf_OkButton();

	private:

		QWidget *centralWidget;

	};
}
