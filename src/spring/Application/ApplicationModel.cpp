#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = std::string("Initial scene");
const std::string baseSceneName = std::string("Base scene");

namespace Spring
{
	ApplicationModel::ApplicationModel():IApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		m_initialScene = std::make_shared<InitialScene>(initialSceneName);
		m_secondScene = std::make_shared<BaseScene>(baseSceneName);

		m_Scenes.emplace(initialSceneName, m_initialScene.get());
		m_Scenes.emplace(baseSceneName, m_secondScene.get());
	}
	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}
	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 

		std::string applicationName = "Undefined";
		m_TransientData.emplace("ApplicationName", applicationName);

		unsigned int sampleRate = 25600;
		m_TransientData.emplace("SampleRate", sampleRate);

		double displayTime = 0.1;
		m_TransientData.emplace("DisplayTime", displayTime);

		double refreshRate = 1.0;
		m_TransientData.emplace("RefreshRate", refreshRate);
	}
}
